/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Roman
 *
 * Created on March 7, 2018, 9:31 PM
 */

#include <cstdlib>
#include "choice_situation.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <fstream>
#include <omp.h>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <sstream>
#include <linux/random.h>
#include <ctime> // Needed for the true randomization
#include <omp.h>
#include "likelihood.h"
#include "equality.h"
#include "blp_share.h"
#include "equality_blp.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
//    ############
//    test openMP
    {
//        vector<double> tmp(1e8, 1.0);
//        double start = omp_get_wtime();
//        omp_set_num_threads(4);
//    #pragma omp parallel for
//        for(int i=0; i<tmp.size(); ++i){
//            tmp[i] = exp(tmp[i]);
//        }
//        cout<<"time to compute exponents " << omp_get_wtime() - start<<endl;
    }
    vector<double> q_agg, q_predict(2737,0.0);
    vector<int> last_idx_agg, ch_agg; // ch_agg is which row corresponds to which school loas_idx.. is how many rows is there in each choice set
    vector<vector<double>> xij_vec;
    
    int num_types = 10;

    int max_row_sets = 200000;
    if(argc == 2){
        max_row_sets = stoi(argv[1]);
    }
//    there is 2737 schools in eligible schools
//    first, read market shares and choice sizes
    ifstream infile; 
    infile.open("q_agg_1.csv");
    vector<double> shares_agg;
    if(infile.is_open()){
        string line;
        int row = 0;
        while((infile.good())){
            getline(infile, line);
            if(!line.empty()){
                shares_agg.push_back(stod(line));
            }
        }
        
    }
    
    {
//        normilize to 1 shares
        double sh_sum = 0;
        for(auto it:shares_agg){
            sh_sum += it;
        }
        for(int i=0; i< shares_agg.size(); ++i){
            shares_agg[i] /=sh_sum;
        }
    }
    q_agg = shares_agg;
    infile.close();
    cout<<"read qagg \n";
    
    infile.open("last_idx_agg_1.csv");
    vector<int> choice_sizes;
    if(infile.is_open()){
        string line;
        int row = 0;
        while((infile.good()) and (row++ <max_row_sets)){
            getline(infile, line);
            if(!line.empty()){
                choice_sizes.push_back(stoi(line));
                last_idx_agg.push_back(stoi(line));
            }
        }
        
    }
    infile.close();
    cout<< "read last idx \n";
//    read X and school ids according to choice sizes just read
    ifstream xij, chagg;
    xij.open("xij_1.csv");
    chagg.open("chagg_1.csv");
//    create vector of choice situations
    vector<choice_situation> situations;
//    read as many lines for each choice situation as dictated by choice_sizes[i]
    
    for(auto size_ch : choice_sizes){
//        cout<<"\n";
//        create matrix of X and school identities 
        vector<vector<double>> X_i;
        vector<int> schools_i;
        
//        strings to keep results of readings from the two files
        string Xs, sch;
        bool use_situation = true; 
        for(int i = 0; i<size_ch; ++i){
//            create vector of characteristics of ith product
            vector<double> X_prod;
            getline(xij, Xs);
            getline(chagg, sch);
            if(sch.empty()) {
                sch = "2737";
//                use_situation = false;
            }
//            sch is ready to be puched back to schools_i
//            cout<< i<<" "<< sch <<" ";
            schools_i.push_back(stoi(sch));
            ch_agg.push_back(stoi(sch));
//            cout<< i <<endl;    
//            need to parse Xs to get values 
            stringstream X_values(Xs);
            string X_value;
            while(getline(X_values,X_value,',')){
                X_prod.push_back(stod(X_value));
            }
            X_i.push_back(X_prod);
            xij_vec.push_back(X_prod);
        }
        
//        create choice situatio of the read X and school ids
//        choice_situation sit_i(X_i, schools_i, 2737);
        if(use_situation){
            situations.push_back(choice_situation(X_i, schools_i, 2737));
        }
    }
    cout<< "read others stuff \n";
//    create dummy vector of beta
    vector<double> beta(11,1);
    double start_time = omp_get_wtime();
//    omp_set_num_threads(5);
//    vector<double> prob = situations[4].probab(beta);
//    cout<<"probability vector\n";
//    double s = 0;
//    for(auto it : prob){
//        if(it>0)
//        cout<<it<<endl;
//        s+=it;
//    }
//    cout<< " sum shares " << s<<endl;
//    return 0;
//    cout<<"time to calculate shares of all choice " << omp_get_wtime() - start_time<<endl;
    likelihood lik1(situations, num_types, situations[0].get_num_covariates(), situations[0].get_num_schools());
    vector<double> beta1; 
    // first num_types*(situations[0].get_num_covariates()) are type specific beta that you multiply by X, then num_types of type specific dummy variables which are redundant, then num_types weights of each type, then 2737 dummies for school
//    generate random double beta
    for (int j=0; j<num_types*(situations[0].get_num_covariates()+2) + 2737; ++j){
//        beta1.push_back(1);
        beta1.push_back((double)rand  () / RAND_MAX);
    }
//    normalize weights to one
    {
        double sum=0;
        for(int i=num_types*(situations[0].get_num_covariates()+1); i<num_types*(situations[0].get_num_covariates()+2); ++i){
            sum += beta1[i];
        }
        for(int i=num_types*(situations[0].get_num_covariates()+1); i<num_types*(situations[0].get_num_covariates()+2); ++i){
            beta1[i] /= (sum/1);
        }
    }
//    nullify the type specific dummies
    for(int i=num_types*(situations[0].get_num_covariates()); i<num_types*(situations[0].get_num_covariates()+1); ++i){
        beta1[i] = 0;
    }
    
//    create random shares of each school
    vector<double> shares(shares_agg);
    vector<double> delta1(beta1.begin()+num_types*(situations[0].get_num_covariates()+2), beta1.end());
    double sum1 = 0;
    {
        double sum=0;
        for(int i=0; i<2737; ++i){
//            shares.push_back((double) rand()/RAND_MAX);
            sum += shares[i];
        }
        
        for(int i=0; i<2737; ++i){
//            shares[i] /= (sum);
            
            sum1 += shares[i];
        }
        cout<< "sum shares " << sum1<<endl;
    }
    cout<< " num situations " << situations.size()<<endl;
    lik1.set_shares(shares_agg);
    double st = omp_get_wtime();    
    std::vector<std::vector<double>>  jac1, jac2, jac;

    vector<double> pred_sh = lik1.blp_share(beta1);
    cout<< "time to calculate shares " << omp_get_wtime() - st<<endl;
    blp_share blp1(xij_vec, last_idx_agg, ch_agg, 2737, num_types);
    
    st = omp_get_wtime();
    vector<double> sh3 = blp1.predict_shares(beta1);
    cout<< "time to calculate shares blp " << omp_get_wtime() - st<<endl;
    
    st = omp_get_wtime();
//    calculate brutforce way
    int first_school = 0;
//    for each choice situation 
    
    double sum_q=0, sum_p = 0, sum_new = 0;
    for(int k=0; k<q_predict.size(); ++k){
        if(k>pred_sh.size() - 11){
//            cout<< pred_sh[k]<< "\t" << sh3[k]<< "\t" <<jac1[130+k][130+k]<< " \t" << jac2[k][k]<<endl;
        }
        
        sum_p+= pred_sh[k];
        sum_new += sh3[k];
    }
    beta1[beta1.size()-2] += 1e-6;
    vector<double> pred_sh1 = lik1.blp_share(beta1);
    cout<<" q predict sum " << sum_q << " " << sum_p<<" " <<sum_new<<"\t"<< (-pred_sh[pred_sh.size()-2] + pred_sh1[pred_sh1.size()-2])*1e6 <<endl;
    
    equality_blp eq_blp1(&blp1, beta1, shares_agg);
    
    {
        std::vector<double> c;
        std::vector<double> objGrad;
        std::vector<double> jac;
        
        double ob1 = eq_blp1.evaluateFC(delta1, c, objGrad, jac);
        
//        ob1 = c[0];
//        delta1[0] += 1e-6;
//        double ob2 = eq1.evaluateFC(delta1, c, objGrad, jac);
//        ob2 = c[0];
//        delta1[0] -= 1e-6;
//        eq1.evaluateGA(delta1, objGrad, jac);
//        cout<< " first constr jac " << jac[0]<< " jacobian size " << jac.size()<<endl;
        cout<< "initial guess \n";
        delta1 = shares_agg;
        for(int i = 0; i< delta1.size(); ++i){
            delta1[i] = log(shares_agg[i]);
        }
        eq_blp1.setXInitial(delta1);
        knitro::KTRSolver solver1(&eq_blp1, KTR_GRADOPT_EXACT, KTR_HESSOPT_BFGS);
        cout<<"here\n";

        solver1.setParam(KTR_PARAM_ALG, 3); //1- barrier 2 = CG algorithm 3 = active set
//        int num_iter = x.size()*5;
        solver1.setParam(KTR_PARAM_MAXIT, 50); // number of iterations 3 times bigger than the number of products.
        solver1.setParam(KTR_PARAM_FTOL, 1e-10);
        solver1.setParam(KTR_PARAM_XTOL, 1e-10);
        solver1.setParam(KTR_PARAM_OPTTOL, 1e-10);
        solver1.setParam(KTR_PARAM_DERIVCHECK, 0);
        solver1.setParam(KTR_PARAM_BAR_MURULE, 2); // set murule to adaptive
//        solver1.solve();
        cout<<"answer \n";
        std::vector<double> solution = solver1.getXValues();
        
        double dpd = 1;
        while(dpd > 1e-2){
            vector<double> sh = eq_blp1.share_predict(delta1);
            vector<double> sol_old(delta1);
            double max = 0;
            for(int i=0; i<sh.size(); ++i){
                delta1[i] += log(shares[i]) - log(sh[i]);
                if(sh[i] ==0 ){
                    cout<< "ith share is zero " <<i<<endl;
                }
                if(abs(log(shares[i]) - log(sh[i])) > max){
                    max = abs(log(shares[i]) - log(sh[i]));
//                    cout<< i<< "\t" <<delta1[i]<< "\t"<< shares[i] << "\t" << sh[i]<<endl;
                }
            }
            dpd = max;
            cout<<"max difference " << max<<endl;
//            for 
            
        }
//        return 0;
        eq_blp1.evaluateFC(solution, c, objGrad, jac);
        eq_blp1.setXInitial(delta1);
        
        knitro::KTRSolver solver2(&eq_blp1, KTR_GRADOPT_EXACT, KTR_HESSOPT_BFGS);
        cout<<"here\n";

        solver2.setParam(KTR_PARAM_ALG, 2); //1- barrier 2 = CG algorithm 3 = active set
//        int num_iter = x.size()*5;
        solver2.setParam(KTR_PARAM_MAXIT, 50); // number of iterations 3 times bigger than the number of products.
        solver2.setParam(KTR_PARAM_FTOL, 1e-12);
        solver2.setParam(KTR_PARAM_XTOL, 1e-12);
        solver2.setParam(KTR_PARAM_OPTTOL, 1e-12);
        solver2.setParam(KTR_PARAM_DERIVCHECK, 0);
//        solver1.setParam(KTR_PARAM_BAR_MURULE, 2); // set murule to adaptive
        solver2.solve();
        {
            int i=0;
            solution = solver2.getXValues();
            vector<double> sh = eq_blp1.share_predict(solver2.getXValues());
            for(i=36 ; i<sh.size(); i += 100){
                cout<<shares[i]<<"\t"<< sh[i]<< "\t" << solution[i]<< endl;
            }
        }
        return 0;
    }
    
    return 0;
    
    
    
//    check that there is no zero predicted shares
    for(auto it : pred_sh){
        if (it ==0){
            cout<< "zero predicted share. not all schools appeared on choice sets \n";
        }
    }
//    return 0;
    
    equality eq1(&lik1, beta1);
    {
        std::vector<double> c;
        std::vector<double> objGrad;
        std::vector<double> jac;
        
        double ob1 = eq1.evaluateFC(delta1, c, objGrad, jac);
        
//        ob1 = c[0];
//        delta1[0] += 1e-6;
//        double ob2 = eq1.evaluateFC(delta1, c, objGrad, jac);
//        ob2 = c[0];
//        delta1[0] -= 1e-6;
//        eq1.evaluateGA(delta1, objGrad, jac);
//        cout<< " first constr jac " << jac[0]<< " jacobian size " << jac.size()<<endl;
        cout<< "initial guess \n";
        delta1 = lik1.get_shares();
        for(int i = 0; i< delta1.size(); ++i){
            delta1[i] = log(shares[i]);
        }
        eq1.setXInitial(delta1);
        knitro::KTRSolver solver1(&eq1, KTR_GRADOPT_EXACT, KTR_HESSOPT_BFGS);
        cout<<"here\n";

        solver1.setParam(KTR_PARAM_ALG, 1); //1- barrier 2 = CG algorithm 3 = active set
//        int num_iter = x.size()*5;
        solver1.setParam(KTR_PARAM_MAXIT, 50); // number of iterations 3 times bigger than the number of products.
        solver1.setParam(KTR_PARAM_FTOL, 1e-12);
        solver1.setParam(KTR_PARAM_XTOL, 1e-12);
        solver1.setParam(KTR_PARAM_OPTTOL, 1e-12);
        solver1.setParam(KTR_PARAM_DERIVCHECK, 0);
        solver1.setParam(KTR_PARAM_BAR_MURULE, 2); // set murule to adaptive
//        solver1.solve();
        cout<<"answer \n";
        std::vector<double> solution = solver1.getXValues();
        
        double dpd = 1;
        while(dpd > 1e-10){
            vector<double> sh = eq1.blp_share(delta1);
            vector<double> sol_old(delta1);
            double max = 0;
            for(int i=0; i<sh.size(); ++i){
                delta1[i] += log(shares[i]) - log(sh[i]);
                if(sh[i] ==0 ){
                    cout<< "ith share is zero " <<i<<endl;
                }
                if(abs(log(shares[i]) - log(sh[i])) > max){
                    max = abs(log(shares[i]) - log(sh[i]));
//                    cout<< i<< "\t" <<delta1[i]<< "\t"<< shares[i] << "\t" << sh[i]<<endl;
                }
            }
            dpd = max;
            cout<<"max difference " << max<<endl;
//            for 
            
        }
//        return 0;
        eq1.evaluateFC(solution, c, objGrad, jac);
        eq1.setXInitial(delta1);
        
        knitro::KTRSolver solver2(&eq1, KTR_GRADOPT_EXACT, KTR_HESSOPT_BFGS);
        cout<<"here\n";

        solver2.setParam(KTR_PARAM_ALG, 2); //1- barrier 2 = CG algorithm 3 = active set
//        int num_iter = x.size()*5;
        solver2.setParam(KTR_PARAM_MAXIT, 50); // number of iterations 3 times bigger than the number of products.
        solver2.setParam(KTR_PARAM_FTOL, 1e-12);
        solver2.setParam(KTR_PARAM_XTOL, 1e-12);
        solver2.setParam(KTR_PARAM_OPTTOL, 1e-12);
        solver2.setParam(KTR_PARAM_DERIVCHECK, 0);
//        solver1.setParam(KTR_PARAM_BAR_MURULE, 2); // set murule to adaptive
        solver2.solve();
        {
            int i=0;
            solution = solver2.getXValues();
            vector<double> sh = eq1.blp_share(solver2.getXValues());
            for(i=36 ; i<sh.size(); i += 100){
                cout<<shares[i]<<"\t"<< sh[i]<< "\t" << solution[i]<< endl;
            }
        }
        return 0;
    }
    cout<<"computed the share of equality \n";
    cout<<"num covariates " << situations[0].get_num_covariates()<<endl<< "beta \n";
//    for(auto it : beta1){

//    cout<<"num covariates " << situations[0].get_num_covariates()<<endl<< "beta \n";
//    for(auto it : beta1){ 

//        cout<<it<<endl;
//    }
    start_time = omp_get_wtime();
    vector<double> sh1 = lik1.blp_share(beta1);
    cout<<"time to compute without erivative " << omp_get_wtime() - start_time<<endl;
    start_time = omp_get_wtime();
    
    lik1.blp_share(beta1, jac);
    cout<< "time to compute shares " << omp_get_wtime() - start_time<<endl;
    
    
    beta1[beta1.size()-1] += 1e-6;
    vector<double> sh2 = lik1.blp_share(beta1);
    cout<< "numerical derivative " << (sh2.back() - sh1.back())*1e6<< " share " << sh2[sh2.size() - 1]<<endl;
    beta1[beta1.size()-1] -= 1e-6;
    beta1[beta1.size()-2] += 1e-6;
    sh2 = lik1.blp_share(beta1);
    cout<< "numerical derivative 2 " << (sh2[sh2.size()-1] - sh1[sh1.size()-1])*1e6<<endl;
    beta1[beta1.size()-2] -= 1e-6;
    beta1[beta1.size()-3] += 1e-6;
    sh2 = lik1.blp_share(beta1);
    cout<< "numerical derivative 3 " << (sh2[sh2.size()-3] - sh1[sh1.size()-3])*1e6<<endl;
            
    return 0;
}

