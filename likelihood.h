/*
 * likelihood contains as member a vector of choice situations, number of types, number of cavariates
 * each type has its own dummy variable
 * main purpose is to estimate likelihood function as a function of number of groups,
 * parameters of utility and shares of each group which is all part of beta vector
 */

/* 
 * File:   likelihood.h
 * Author: Roman
 *
 * Created on March 8, 2018, 1:02 AM
 */

#ifndef LIKELIHOOD_H
#define LIKELIHOOD_H
#include "choice_situation.h"
//#include </opt/rh/devtoolset-4/root/usr/include/c++/5.3.1/vector>
#include <vector>

class likelihood {
public :
    likelihood();
    
//    constructor from vector of situations and number of types and number of covariates
    likelihood(std::vector<choice_situation> situations1, int num_ty, int num_cov, int num_sch) : situations(situations1), num_types(num_ty), num_covariates(num_cov), total_number_of_schools(num_sch) {};
    
//    computes likelihood of a given set of choices with given beta values version with jacobian also calculates jacobian
    std::vector<double> blp_share(std::vector<double> beta, std::vector<std::vector<double>> & jac );
    std::vector<double> blp_share(std::vector<double> beta );
    void set_shares(std::vector<double> sh) {
        shares_data = sh;
    }
    
    std::vector<double> get_shares() {
        return shares_data;
    }
    
    int get_num_types(){
        return num_types;
    }
    
    int get_num_covariates(){
        return num_covariates;
    }
    int get_total_number_of_schools(){
        return total_number_of_schools;
    }
    
    
    
private :
    std::vector<choice_situation> situations;
    int num_types;
    int num_covariates;
    int total_number_of_schools;
    std::vector<double> shares_data;

};

#endif /* LIKELIHOOD_H */

