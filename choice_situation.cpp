/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   choice_situation.cpp
 * Author: Roman
 * 
 * Created on March 7, 2018, 9:33 PM
 */

#include "choice_situation.h"
#include<iostream>
#include<map>
#include<vector>
#include "/storage/home/rji5040/work/Tasmanian_run/include/TasmanianSparseGrid.hpp"
#include <utility>
#include <c++/5.3.1/cmath>
#include <math.h>
#include <algorithm>    // std::is_sorted, std::prev_permutation
#include <array>  
#include <boost/math/distributions/lognormal.hpp>
#include <omp.h>

using namespace std;

vector<double> choice_situation::probab(vector<double> beta){
//    check that beta has the same size as X[0]
    if(beta.size() != X[0].size()){
        throw runtime_error("size of beta is different from the size of X[0]");
    }
    
    //    construct vector of zeros of the size of number of schools
    vector<double> probability(total_number_of_schools,0.0);
    
//    check if there is only one school in choice set
    vector<int> unique_schools(school_id);
    sort( unique_schools.begin(), unique_schools.end() );
    unique_schools.erase(unique(unique_schools.begin(),unique_schools.end()), unique_schools.end());
    if(unique_schools.size()==1){
        probability[unique_schools[0]] = 1;
        return probability;
    }
    

    
//    calculate deltas
    vector<double> delta_prod;
//    for each product compute scalar product of its characteristics and beta
    for(auto prod : X){
        double delta_i=0;
        for(int i=0; i<prod.size(); ++i){
            delta_i += prod[i]*beta[i];
        }
        delta_prod.push_back(delta_i);
    }
//    cout<< "sizes of delta vertical and delta_prod " << delta_vertical.size() << " " <<delta_prod.size()<<endl;
//    add vertical compoenent to each school quality if it is set
    for(int i=0; i<delta_prod.size(); ++i){
//        cout<< "school id " << school_id[i] << i<<endl;
        delta_prod[i] += delta_vertical[school_id[i]];
    }
    
//    make a copy of delta to compute denominator, add there minus dummy for eoutside option
    vector<double> delta_denominator(delta_prod);
    
//    there is no outside optin in this setting.
//    delta_denominator.push_back(-dummy);
    
    
//    sort delta_denominator in descending order
    sort(delta_denominator.begin(), delta_denominator.end());
    reverse(delta_denominator.begin(), delta_denominator.end());
    
//    compute denominator
//    sum_i(exp(delta_i)) = exp(delta_0)*(sum_i(exp(delta_i-delta_0)))
    double denominator=0;
    int i=0;
//    we only need elements that do not differ from the biggest element in delta_denominator more than by 20
    while((delta_denominator[i] - delta_denominator[0]>-20) & (i<delta_denominator.size())){
        denominator += exp(delta_denominator[i++] - delta_denominator[0]);
    }
    denominator *= exp(delta_denominator[0]);
    
//    compute shares and assign them to the specific elements of probability vector
    
    for(int i=0; i< delta_prod.size(); ++i){
        probability[school_id[i]] += exp(delta_prod[i])/denominator;
    }
    
    
    
    return probability;
}


