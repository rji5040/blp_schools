/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   blp_share.cpp
 * Author: Roman
 * 
 * Created on March 23, 2018, 10:01 PM
 */

#include "blp_share.h"
#include <vector>
#include <cmath>
#include <algorithm>
#include <omp.h>

using namespace std;

vector<double> blp_share::predict_shares(vector<double> & beta){
//    make template for predicted probabilities
    vector<double> shares(num_schools, 0.0);
    
//    compute vector of deltas for each type and each school outer index indexes types, inner index indexes number of row in xij 
    vector<vector<double>> delta_combined;
//    first copy the vertical part of utility from the corresponding position in beta vector
    vector<double> delta_vertical;
    for(int i=0; i<schools_id.size(); ++i){
        delta_vertical.push_back(beta[num_types*(xij[0].size() + 2) + schools_id[i]]);
    }
//    push vertical delta to vector of type specific deltas, make num_type copies
    for(int i=0; i< num_types; ++i){
        delta_combined.push_back(delta_vertical);
    }
//    for each type add beta X to delta 
#pragma omp parallel for schedule(dynamic, 1) num_threads(10)
    for(int type=0; type<num_types; ++type){
//         for each row in delta add inner product of corresponding beta and X
        for(int i=0; i<delta_combined[type].size(); ++i){
//            x_dim is a dimension of observed heterogeneity
            for(int x_dim = 0; x_dim < xij[i].size(); ++x_dim){
                delta_combined[type][i] += xij[i][x_dim]*beta[type*xij[0].size() + x_dim];
            }
        }
    }
    
//    for each type compute probabilities for each school at each choice set
//make temporary array that holds shares of different schedules
    
    
#pragma omp parallel num_threads(10) 
    {
        double shares_tmp[num_schools] = {};
#pragma omp for schedule(dynamic, 1)
        for(int type = 0; type < num_types; ++type){

            int last_row = 0;
    //        go ever all choice situations and compute denominator for each

            for(int choice = 0; choice<idx.size(); ++choice){

    //            check that there is more than one school in set (deemed not speeding up calculations)
    //            {
    //                vector<int> unique_schools(schools_id.begin() + last_row, schools_id.begin()+ last_row + idx[choice]);
    //                sort(unique_schools.begin(), unique_schools.end());
    //                unique_schools.erase(unique(unique_schools.begin(), unique_schools.end()), unique_schools.end());
    //                if(unique_schools.size() < 2){
    //                    shares[schools_id[unique_schools[0]]] += 1*beta[num_types*(xij[0].size()+1)+type];
    //                    continue;
    //                }
    //            }

    //        for each choice situation copy relevant deltas from delta_combined
                vector<double> denom_members(delta_combined[type].begin() + last_row, delta_combined[type].begin() + last_row + idx[choice]);
                
    //            create a copy of denom members to compute shares later
//                vector<double> num_members(denom_members);

    //            sort the deltas and choose only those that differ from the largest one no more than by 20
    //            sort(denom_members.begin(), denom_members.end());
    //            reverse(denom_members.begin(), denom_members.end());
    //            int pointer_small = 0;
    //            while(((denom_members[0] - denom_members[pointer_small])<20) & (pointer_small < denom_members.size())){
    //                ++pointer_small;
    //            }
    ////            remove the small members from denom calculation
    //            denom_members.erase(denom_members.begin() + pointer_small, denom_members.end());
    //            compute denominator without small members
                double denom = 0.0;
                for(auto it : denom_members){
    //                denominators[type][choice] += exp(it);
                    denom += exp(it);
                }

    //            compute each school's share and add it with weight to corresponding dimension of shares

                for(int i=0; i<denom_members.size(); ++i){
//                    double tmp = (exp(num_members[i])/denom)*beta[num_types*(xij[0].size()+1)+type];
                    shares_tmp[schools_id[last_row + i]] += (exp(denom_members[i])/denom)*beta[num_types*(xij[0].size()+1)+type];
                }

                last_row += idx[choice];
            }
        }
//        sum up the computed shares
#pragma omp critical
        {
            for(int i=0; i<num_schools; ++i){
                shares[i] += shares_tmp[i];
            }
        }
    }
    
//    devide shares by number of choices
    int i=0;
    for(i = 0; i<num_schools; ++i){
        shares[i] /= idx.size();
    }
    
    return shares;
    
}


vector<double> blp_share::predict_shares(vector<double> & beta, std::vector<std::vector<double>> & jac){
    
//    erase whatever is stored in jac and replace it with zeros
    jac = vector<vector<double>>(num_schools, vector<double> (num_schools,0.0));
    
//    make template for predicted probabilities
    vector<double> shares(num_schools, 0.0);
    
//    compute vector of deltas for each type and each school outer index indexes types, inner index indexes number of row in xij 
    vector<vector<double>> delta_combined;
//    first copy the vertical part of utility from the corresponding position in beta vector
    vector<double> delta_vertical;
    for(int i=0; i<schools_id.size(); ++i){
        delta_vertical.push_back(beta[num_types*(xij[0].size() + 2) + schools_id[i]]);
    }
//    push vertical delta to vector of type specific deltas, make num_type copies
    for(int i=0; i< num_types; ++i){
        delta_combined.push_back(delta_vertical);
    }
    
//    for each type add beta X to delta 
#pragma omp parallel num_threads(10) 
    {
        
#pragma omp  for schedule(dynamic, 1) 
    for(int type=0; type<num_types; ++type){
//         for each row in delta add inner product of corresponding beta and X
        for(int i=0; i<delta_combined[type].size(); ++i){
//            x_dim is a dimension of observed heterogeneity
            for(int x_dim = 0; x_dim < xij[i].size(); ++x_dim){
                delta_combined[type][i] += xij[i][x_dim]*beta[type*xij[0].size() + x_dim];
            }
        }
    }
    
//    for each type compute probabilities for each school at each choice set
//make temporary array that holds shares of different schedules
    
//        on machine with openmp 4.5 support it can be avioded by using reduction pragma for arrays of double.
        double shares_tmp[num_schools] = {};
        vector<vector<double>> jac_tmp(num_schools, vector<double>(num_schools, 0.0));
        
#pragma omp for schedule(dynamic, 1)
        for(int type = 0; type < num_types; ++type){
            
            int last_row = 0;
    //        go ever all choice situations and compute denominator for each

            for(int choice = 0; choice<idx.size(); ++choice){

    //            check that there is more than one school in set (deemed not speeding up calculations)
    //            {
    //                vector<int> unique_schools(schools_id.begin() + last_row, schools_id.begin()+ last_row + idx[choice]);
    //                sort(unique_schools.begin(), unique_schools.end());
    //                unique_schools.erase(unique(unique_schools.begin(), unique_schools.end()), unique_schools.end());
    //                if(unique_schools.size() < 2){
    //                    shares[schools_id[unique_schools[0]]] += 1*beta[num_types*(xij[0].size()+1)+type];
    //                    continue;
    //                }
    //            }

    //        for each choice situation copy relevant deltas from delta_combined
                vector<double> denom_members(delta_combined[type].begin() + last_row, delta_combined[type].begin() + last_row + idx[choice]);
    //            create a copy of denom members to compute shares later
//                vector<double> num_members(denom_members);

    //            sort the deltas and choose only those that differ from the largest one no more than by 20
    //            sort(denom_members.begin(), denom_members.end());
    //            reverse(denom_members.begin(), denom_members.end());
    //            int pointer_small = 0;
    //            while(((denom_members[0] - denom_members[pointer_small])<20) & (pointer_small < denom_members.size())){
    //                ++pointer_small;
    //            }
    ////            remove the small members from denom calculation
    //            denom_members.erase(denom_members.begin() + pointer_small, denom_members.end());
    //            compute denominator without small members
                double denom = 0.0;
                for(auto it : denom_members){
    //                denominators[type][choice] += exp(it);
                    denom += exp(it);
                }

    //            compute each school's share and add it with weight to corresponding dimension of shares
                vector<double> shares_tmp_tmp(denom_members.size(),0.0);
                for(int i=0; i<denom_members.size(); ++i){
//                    compute temporary shares of schools in this choice set to later compute jacobian
                    shares_tmp_tmp[i] += (exp(denom_members[i])/denom);
                }
                
//                add up the shares with weights and compute jacobian
                for(int i=0; i<denom_members.size(); ++i){
//                    double tmp = (exp(num_members[i])/denom)*beta[num_types*(xij[0].size()+1)+type];
                    shares_tmp[schools_id[last_row + i]] += shares_tmp_tmp[i]*beta[num_types*(xij[0].size()+1)+type];
                    for(int j=0; j<denom_members.size(); ++j){
                        if(i==j){
                            jac_tmp[schools_id[last_row + i]][schools_id[last_row + j]] += shares_tmp_tmp[i]*(1-shares_tmp_tmp[j])*beta[num_types*(xij[0].size()+1)+type];
                        }
                        else{
                            jac_tmp[schools_id[last_row + i]][schools_id[last_row + j]] += shares_tmp_tmp[i]*(-shares_tmp_tmp[j])*beta[num_types*(xij[0].size()+1)+type];
                        }
                    }
                }
//                update last row for the next choice set
                last_row += idx[choice];
            }
        }
        
//        sum up the computed shares
#pragma omp critical
        {
            for(int i=0; i<num_schools; ++i){
                shares[i] += shares_tmp[i];
                for(int j=0; j<num_schools; ++j){
                    jac[i][j] += jac_tmp[i][j]/idx.size();
                }
            }
        }
    }
    
//    devide shares by number of choices
    int i=0;
    for(i = 0; i<num_schools; ++i){
        shares[i] /= idx.size();
    }
    
    return shares;
    
}