/*
 * Choice situation is one choice instance of a person. It has members:
 * 1) matrix of attributes of each alternative X
 * 2) identity of each row in X with a particular school number
 * 
 * Main purpose of this class is to produce vector of probabilities to choose each product. 
 * That vector is of the size of full number of schools with only nonzeros on positions where 
 */

/* 
 * File:   choice_situation.h
 * Author: Roman
 *
 * Created on March 7, 2018, 9:33 PM
 */

#ifndef CHOICE_SITUATION_H
#define CHOICE_SITUATION_H
#include<iostream>
#include<vector>

#include <math.h>
#include <algorithm>    // std::is_sorted, std::prev_permutation
#include <array> 


class choice_situation {
public:
    choice_situation() : total_number_of_schools(0), dummy(0) {};
    
    
//    constructor from X and school ids
    choice_situation(std::vector<std::vector<double> > X1, std::vector<int> school_id1, int num_sch) : X(X1), school_id(school_id1), total_number_of_schools(num_sch), dummy(0), 
            delta_vertical(std::vector<double>(num_sch,0.0)){ };
    
    std::vector<double> probab(std::vector<double> beta);
    
//    gets number of covariates in this choice situation
    int get_num_covariates(){
        return X[0].size();
    }
    
    std::vector<std::vector<double>> get_X(){
        return X;
    }
    
//    sets dummy variable for this choice situation
    void set_dummy(double dumm){
        dummy = dumm;
    }
    void set_delta_vertical( std::vector<double> dlt) {
        delta_vertical = dlt;
    }
    
    int get_num_schools(){
        return total_number_of_schools;
    }
    
private:
    std::vector<std::vector<double> > X;
    std::vector<int> school_id;
    int total_number_of_schools;
    double dummy; //dummy of a type irrelevent when no outside option
    std::vector<double> delta_vertical; // vertical characteristics of each school

};

#endif /* CHOICE_SITUATION_H */

