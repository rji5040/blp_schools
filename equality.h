/*
 * This class is implementing blp market inversion by minimizing sum of squared deviations.
 * It has as member likelihood address that has all the information on the market shares and its derivatives
 * it also has as member part of beta that we condition on when inveting market shares
 */

/* 
 * File:   equality.h
 * Author: Roman
 *
 * Created on March 12, 2018, 3:23 PM
 */

#ifndef EQUALITY_H
#define EQUALITY_H

#include "likelihood.h"
#include <vector>
#include <map>
#include "TasmanianSparseGrid.hpp"
#include <omp.h>
#include "knitro.h"
#include "KTRSolver.h"
#include "KTRProblem.h"

class equality : public knitro::KTRProblem{
public:
    equality(likelihood * lik, std::vector<double> b) : KTRProblem(lik->get_total_number_of_schools(), 2737), likl(lik), beta_nl(std::vector<double> (b.begin(), b.begin() + (lik->get_num_covariates() + 2)*lik->get_num_types())){ 
        setObjectiveProperties();
        setVariableProperties();
        setConstraintProperties();
    };
    
    void setObjectiveProperties() {
        setObjType(knitro::KTREnums::ObjectiveType::ObjGeneral);
        setObjGoal(knitro::KTREnums::ObjectiveGoal::Minimize);
    }

    // variable bounds. All variables 0 <= x.
    void setVariableProperties() {
        setVarLoBnds(-KTR_INFBOUND);
        setVarUpBnds( KTR_INFBOUND);
    }

    // constraint properties
    void setConstraintProperties() {
        // set constraint types
        setConTypes(knitro::KTREnums::ConstraintType::ConGeneral);
        setConTypes(1, knitro::KTREnums::ConstraintType::ConGeneral);

        // set constraint lower bounds to zero for all variables
        setConLoBnds(0.0);

        // set constraint upper bounds
        setConUpBnds(0.0);
//        setConUpBnds(1, KTR_INFBOUND);
    }
    
    
//    functione needed by knitro to solve for the shares inversion.
    double evaluateFC(const std::vector<double>& delta,  std::vector<double>& c,  std::vector<double>& objGrad, std::vector<double>& jac){//, std::vector<double> & p, std::vector<std::vector<double>> x, double sigma_p, std::vector<double> sigma_x) {
        std::vector<std::vector<double>> jacobian;
        
//        delta is a vector of individual school dumies 
//        
//        create vector of beta that can be fed to blp shares function
//        first, copy nonlinear part of beta that we condition on. it does not change.
        std::vector<double> beta(beta_nl);
//        now ad there the changing part delta from parameter of this function
        beta.insert(beta.end(), delta.begin(), delta.end());
//        create vector of betas 

//        compute market share predicted by model
        std::vector<double> share_predict = likl->blp_share(beta);//(delta, x, p, sigma_p, sigmax, jacobian);
//        std::cout<<"here"<<std::endl;
        double obj=0;
          // constraints
        jac.clear();
        c.clear();
        std::vector<double> shares_data = likl->get_shares();
        for(int i=0; i<shares_data.size(); ++i){
            c.push_back(share_predict[i] - shares_data[i]);
//            obj += (share_predict[i] - shares_data[i])*(share_predict[i] - shares_data[i]);
        }
          // return objective function value
          return obj;
      }
    
    int evaluateGA(const std::vector<double>& delta, std::vector<double>& objGrad, std::vector<double>& jac) {
        std::vector<std::vector<double>> jacobian;
//        delta is a vector of individual school dumies 
//        
//        create vector of beta that can be fed to blp shares function
//        first, copy nonlinear part of beta that we condition on. it does not change.
        std::vector<double> beta(beta_nl);
//        now ad there the changing part delta from parameter of this function
        beta.insert(beta.end(), delta.begin(), delta.end());
//        create vector of betas 

//        compute market share predicted by model
        std::vector<double> share_predict = likl->blp_share(beta, jacobian);//(delta, x, p, sigma_p, sigmax, jacobian);
 
        std::vector<double> shares_data = likl->get_shares();
        jac.clear();
        std::vector<double> obj_tmp(delta.size(),0.0);
        objGrad = obj_tmp;
        for(int i=0; i< shares_data.size(); ++i){
//            std::cout<<"here\n";
//            for(int j=0; j<shares_data.size(); ++j){
//                objGrad[j] += 2*jacobian[(likl->get_num_covariates() + 2)*likl->get_num_types() + i][(likl->get_num_covariates() + 2)*likl->get_num_types() + j]*(share_predict[i] - shares_data[i]);
//            }
            
            for(int j = (likl->get_num_covariates() + 2)*likl->get_num_types(); j< jacobian[i].size(); ++j){
                jac.push_back(jacobian[(likl->get_num_covariates() + 2)*likl->get_num_types() + i][j]);
            }
        }
        return 0;
    }
 

    
//    toy function to call blp shares from member likl reference.
    std::vector<double> blp_share(std::vector<double> beta ){
        std::vector<double> bt(beta_nl);
        bt.insert(bt.end(), beta.begin(), beta.end());
        return likl->blp_share(bt);
    }
    
private:
    likelihood * likl; // likeliood pointer that computes marekt shares and jacobian
    std::vector<double> beta_nl; // nonlinear parameters of blp market shares that we condition on to invert market shares.

};

#endif /* EQUALITY_H */

