/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   likelihood.cpp
 * Author: Roman
 * 
 * Created on March 8, 2018, 1:02 AM
 */

#include "likelihood.h"
#include<iostream>
#include<map>
#include <vector>
#include "/storage/home/rji5040/work/Tasmanian_run/include/TasmanianSparseGrid.hpp"
#include <utility>
#include <c++/5.3.1/cmath>
#include <math.h>
#include <algorithm>    // std::is_sorted, std::prev_permutation
#include <array>  
#include <boost/math/distributions/lognormal.hpp>
#include <omp.h>
#include <c++/5.3.1/bits/stl_vector.h>
#include "choice_situation.h"

using namespace std;

likelihood::likelihood(): num_types(1), num_covariates(0) {
}

std::vector<double> likelihood::blp_share(std::vector<double> beta){
//    make template for aggregate shares.
    cout<<"going to compute blp share\n";
    vector<double> ag_shares(situations[0].get_num_schools(),0.0);
//    check that size of beta is compatible with number of types, number of covariates and dummies
//    note difference with original matlab code where type-specific dummy variable played double role of type size.
    if(beta.size() != (num_covariates + 2)*num_types + total_number_of_schools){
        throw runtime_error("size of beta is different from number of covariates plus one times number of types");
    }
    
    vector<omp_lock_t>  lock_function;
//    initialize locks
//    cout<<"locks set \n";
    {
    for(int j=0; j<beta.size(); j++){
        lock_function.push_back(omp_lock_t());
        omp_init_lock(&(lock_function[j]));
    }
    }
    
//    loop over choice situations, for each situation loop over types and calculate likelihood for each type and weight them
    omp_set_num_threads(1);
//#pragma omp parallel for schedule(dynamic, 10) num_threads(1)
    for(int k=0; k<situations.size(); ++k){
//        cout<<k<<endl;
        choice_situation situation = situations[k];
//        loop over types
        for(int i=0; i< num_types; ++i){
//            get relevant part of beta vector and set up the type fixed effect
            std::vector<double> beta_type(beta.begin() + i*num_covariates, beta.begin() + (i+1)*num_covariates);
            
//            set dummy for type
            situation.set_dummy(beta[num_covariates*num_types + i]);
//            cout<<"set dummy \n";
//            set vertical components of each school
            situation.set_delta_vertical(vector<double>(beta.begin()+(num_covariates + 2)*num_types, beta.end()));
//            cout<<"set deltas vertical \n";
//            compute conditional shares and add them up to unconditional with weights
            int j=0;
            for(auto share : situation.probab(beta_type)){
//                cout<< " computed share \n";
//set up lock to element [j]
//                omp_set_lock(&(lock_function[j]));
                ag_shares[j++] += share*beta[(num_covariates+1)*(num_types) + i];
//                omp_unset_lock(&(lock_function[j]));
            }
        }
    }
//    cout<<"shares\n";
//    normalize by the number of choice situations
    for(int i=0; i<ag_shares.size(); ++ i){
        ag_shares[i] /= situations.size();
//        if(it>0)
//        cout<<it<<endl;
    }
    
    return ag_shares;
    
}



std::vector<double> likelihood::blp_share(std::vector<double> beta, std::vector<std::vector<double>> & jac){
//    make jac zero matrix
    jac.clear();
    for(int i=0; i< beta.size(); ++i){
        jac.push_back(vector<double>(beta.size(), 0.0));
    }
//    make template for aggregate shares.
    vector<double> ag_shares(situations[0].get_num_schools(), 0.0);
//    check that size of beta is compatible with number of types, number of covariates and dummies
//    note difference with original matlab code where type-specific dummy variable played double role of type size.
    if (beta.size() != (num_covariates + 2)*num_types + total_number_of_schools){
        throw runtime_error("size of beta is different from number of covariates plus one times number of types");
    }
//    cout<<"setting lock \n";
//    set up locks to prevent writing to same parts of array
    vector<omp_lock_t>  lock_jacobian;
//    initialize locks
//    cout<<"locks set \n";
    {
    for (int i=0; i<beta.size(); i++)
        for(int j=0; j<beta.size(); j++){
            lock_jacobian.push_back(omp_lock_t());
            omp_init_lock(&(lock_jacobian[i*beta.size() + j]));
        }
    }
//    cout<<"locks initialized \n";
//    loop over choice situations, for each situation loop over types and calculate likelihood for each type and weight them
//#pragma omp parallel for schedule dynamic(1)

#pragma omp parallel for schedule(dynamic, 1) num_threads(15)

    for(int k=0; k<situations.size(); ++k){
        choice_situation situation = situations[k];
//        loop over types
        for(int i=0; i< num_types; ++i){
//            get relevant part of beta vector and set up the type fixed effect
            std::vector<double> beta_type(beta.begin() + i*num_covariates, beta.begin() + (i+1)*num_covariates);
//            set dummy for type
            situation.set_dummy(beta[num_covariates*num_types + i]);
//            cout<<"set dummy \n";
//            set vertical components of each school
            situation.set_delta_vertical(vector<double>(beta.begin()+(num_covariates + 2)*num_types, beta.end()));
//            cout<<"set deltas vertical \n";
            
//            compute conditional shares and add them up to unconditional with weights also compute derivatives of each share with respect to each parameter.
            vector<double> share_cond = situation.probab(beta_type);
            for(int j=0; j< share_cond.size(); ++j){
//                only compute the derivative with respect to delta part of utility in betas
//                omp_set_num_threads(2);

                for(int t=0; t<total_number_of_schools; ++t){
//                    if t==j, it is own delta elasticity
                    if(share_cond[j]*share_cond[t] ==0) continue;
                    if(t==j){
                        double tmp1 = share_cond[j]*(1-share_cond[j])*beta[(num_covariates+1)*(num_types) + i];
//                        set lock to element that will be changed
//                        having locks is error-proof, but takes more time ~twice as much. without them seems to work fine
//                        cout<<"setting lock t==j\n";
//                        omp_set_lock(&(lock_jacobian  [(num_covariates + 2)*num_types + j*beta.size() + (num_covariates + 2)*num_types + t]));
//                        cout<<" lock set t==j\n";
                        jac                           [(num_covariates + 2)*num_types + j][(num_covariates + 2)*num_types + t] +=  tmp1;
//                        omp_unset_lock(&(lock_jacobian[(num_covariates + 2)*num_types + j*beta.size() + (num_covariates + 2)*num_types + t]));
//                        cout<<"unsetting lock t==j\n";
                    }
                    else{
                        double tmp1 = -share_cond[j]*share_cond[t]     *beta[(num_covariates+1)*(num_types) + i];
//                        omp_set_lock(&(lock_jacobian  [(num_covariates + 2)*num_types + j*beta.size() + (num_covariates + 2)*num_types + t]));
                        jac                           [(num_covariates + 2)*num_types + j][(num_covariates + 2)*num_types + t] += tmp1;
//                        omp_unset_lock(&(lock_jacobian[(num_covariates + 2)*num_types + j*beta.size() + (num_covariates + 2)*num_types + t]));
                        
                    }
                }
#pragma omp critical
                ag_shares[j] += share_cond[j]*beta[(num_covariates+1)*(num_types) + i];
            }
        }
    }
//    cout<<"shares\n";
//    normalize by the number of choice situations
    for(int i=0; i<ag_shares.size(); ++ i){
        ag_shares[i] /= situations.size();
//        if(it>0)
//        cout<<it<<endl;
    }
    for(int i=0; i<jac.size(); ++i){
        for(int j=0; j<jac[0].size(); ++j){
            jac[i][j] /= situations.size();
        }
    }
//    cout<< "last element of jacobian " << endl;
//    for(int i=2730; i < 2737; ++i){
//        for(int j=2730; j<2737; ++j){
//            cout<<jac[(num_covariates + 2)*num_types+i][(num_covariates + 2)*num_types+j]<<"\t";
//        }
//        cout<<endl;
//    }
    
    return ag_shares;
    
}

