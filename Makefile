CXX      = /opt/rh/devtoolset-4/root/usr/bin/g++
#CXX      = pgc++
SDIR     = .
KIDIR1	 = /opt/aci/sw/knitro/10.2.1/include
KIDIR2	 = /opt/aci/sw/knitro/10.2.1/examples/C++/include/
EIGEN	 = /storage/home/rji5040/work/eigen/
BOOSTI	 = /storage/work/rji5040/boost_1_65_1/
BOOSTL	 = /storage/work/rji5040/boost_1_65_1/stage/lib
TASMANIANL = /storage/home/rji5040/work/Tasmanian_run/lib
TASMANIANI = /storage/home/rji5040/work/Tasmanian_run/include/
# parameters for gurobi
INC      = /storage/home/rji5040/work/gurobi752/linux64/include/
CARGS    = -m64 -g
CLIB     = -L/storage/home/rji5040/work/gurobi752/linux64/lib/ -lgurobi75
CPPLIB   = -L/storage/home/rji5040/work/gurobi752/linux64/lib/ -lgurobi_c++ 
# #-lgurobi75
#
#CXXFLAGS = -O2  -c -I$(SDIR) -I$(IDIR) -I$(KIDIR1) -I$(KIDIR2) -I$(EIGEN) -std=c++0x
CXXFLAGS = -O2 -std=c++14  -c -I$(SDIR)  -I$(KIDIR1) -I$(KIDIR2) -I$(EIGEN) -I. -I$(BOOSTI) -I$(INC) $(CARGS) -I$(TASMANIANI)   -fopenmp
#LDFLAGS  = -lm -L$(LDIR) -lscl   -fopenmp -rdynamic /opt/aci/sw/knitro/10.2.1/lib/libknitro.so -ldl -Wl,-rpath,/opt/aci/sw/knitro/10.2.1/lib
LDFLAGS  = -lm $(CPPLIB)  -L$(BOOSTL) $(BOOSTL)/libboost_serialization.a -fopenmp -rdynamic /opt/aci/sw/knitro/10.2.1/lib/libknitro.so.10.2.1 $(TASMANIANL)/libtasmaniansparsegrid.a -ldl -Wl,-rpath,/opt/aci/sw/knitro/10.2.1/lib 
#LDFLAGS  = -lm $(CPPLIB) -L$(BOOSTL) $(BOOSTL)/libboost_serialization.a -fopenmp -rdynamic 

main : main.o choice_situation.o equality.o likelihood.o blp_share.o equality_blp.o 
	$(CXX) -o main main.o choice_situation.o equality.o likelihood.o blp_share.o equality_blp.o $(LDFLAGS)

main.o : $(SDIR)/main.cpp $(SDIR)/choice_situation.h $(SDIR)/equality.h $(SDIR)/likelihood.h  
	$(CXX) $(CXXFLAGS) -o main.o $(SDIR)/main.cpp
	
choice_situation.o : $(SDIR)/choice_situation.h $(SDIR)/choice_situation.cpp
	$(CXX) $(CXXFLAGS) -o choice_situation.o $(SDIR)/choice_situation.cpp

likelihood.o : $(SDIR)/likelihood.h $(SDIR)/likelihood.cpp $(SDIR)/choice_situation.h
	$(CXX) $(CXXFLAGS) -o likelihood.o $(SDIR)/likelihood.cpp

equality.o : $(SDIR)/equality.h $(SDIR)/equality.cpp $(SDIR)/likelihood.h
	$(CXX) $(CXXFLAGS) -o equality.o $(SDIR)/equality.cpp
	
blp_share.o : $(SDIR)/blp_share.h $(SDIR)/blp_share.cpp
	$(CXX) $(CXXFLAGS) -o blp_share.o $(SDIR)/blp_share.cpp
	
equality_blp.o : $(SDIR)/equality_blp.h $(SDIR)/equality_blp.cpp
	$(CXX) $(CXXFLAGS) -o equality_blp.o $(SDIR)/equality_blp.cpp

clean :
	rm -f *.o
	rm -f main
veryclean :
	rm -f *.o
	rm -f main 
