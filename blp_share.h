/*
 * This class implements the blp shares with all the data needed in one place
 * has members 
 * idx - vector of sizes of each choice set
 * xij - matrix of variable attributes that change with betsas sum(idx) must equal to height of xij and school_id
 * school_id - vector of school id numbers that tells which row of xij corresponds to which school number
 * num_schools - total number of schools 
 * num_types - number of types
 */

/* 
 * File:   blp_share.h
 * Author: Roman
 *
 * Created on March 23, 2018, 10:01 PM
 */

#ifndef BLP_SHARE_H
#define BLP_SHARE_H
#include <vector>
#include <iostream>

class blp_share {
public:
    blp_share(std::vector<std::vector<double>> xij, std::vector<int> idx, std::vector<int> schools_id, int num_schools = 2737, int num_types = 10) : xij(xij), idx(idx), schools_id(schools_id), num_schools(num_schools), num_types(num_types) {};
//    no default constructor. need to construct with actual 
    std::vector<double> predict_shares(std::vector<double> & beta);
    
    std::vector<double> predict_shares(std::vector<double> &beta, std::vector<std::vector<double>> & jac);
    
    int get_total_number_of_schools(){
        return num_schools;
    }
    
    int get_num_covariates(){
        return xij[0].size();
    }
    
    int get_num_types(){
        return num_types;
    }
private:
    std::vector<std::vector<double>> xij;
    std::vector<int> idx;
    std::vector<int> schools_id;
    int num_schools;
    int num_types;
    

};

#endif /* BLP_SHARE_H */

